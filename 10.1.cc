class Solution {
public:
    int jumpFloor(int number) { //跳台阶本质也是Fibonacci
        if(number <= 0)
            return 0;
        if(number == 1)
            return 1;
        if(number == 2)
            return 2;
        int fn_1=2;
        int fn_2=1;
        int fn;
        int i;
        for(i=3;i<=number;++i){
            fn=fn_1+fn_2;
            fn_2=fn_1;
            fn_1=fn;
        }
        return fn;
    }
};