class Solution {
public:
     int  NumberOf1(int n) {
         int nCount=0;
         while(n){
             ++nCount;
             n=n&(n-1);
         }
         return nCount;
     }
};