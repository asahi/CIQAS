#include <stdio.h>

int match(char *str, char *pattern){
    if(!str || !pattern) return 0;
    return matchWorker(str,pattern);
}

int matchWorker(char *str, char *pattern){
    if(*str=='\0'&&*pattern=='\0') return 1;
    if(*str!='\0'&&*pattern=='\0') return 0;
    //next char is * 
    if(*(pattern+1)=='*'){
        if(*str==*pattern || (*str!='\0' && *pattern=='.')){ // possible match 0 or 1 or many times
            return matchWorker(str+1,pattern) || matchWorker(str+1,pattern+2) || matchWorker(str,pattern+2);
            /*many*/                            /*1*/                           /*0*/
        }
        else{//ignore because different
            return matchWorker(str,pattern+2);
        }
    }
    //. or same
    if(*str==*pattern || (*str!='\0' && *pattern=='.')){
        return matchWorker(str+1,pattern+1);
    }
    return 0;
    
}

int main(int argc, char const *argv[])
{
    char str[]="aaa";
    char pattern[]="aa.a";
    if(match(str,pattern)){
        printf("Yes\n");
    }
    else{
        printf("No\n");
    }
    return 0;
}
