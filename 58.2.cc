#include <string>

using namespace std;

class Solution {
public:
    string ret;
    string LeftRotateString(string str, int n) {
        ret.clear();
        int length = str.size();
        if(n<=0 || n==length || str.empty()) return str;
        char * mystr = new char[length+1];
        mystr[length] = '\0';

        if(n>length) n%=length;
        for(int i=0;i<n;++i){
            mystr[length-n+i]=str[i];
        }

        for(int i=n;i<length;++i){
            mystr[i-n]=str[i];
        }
        ret=mystr;
        delete[] mystr;
        return ret;
    }
};


int main(int argc, char const *argv[])
{
    string str = "testABC1234";
    Solution s;
    string ret = s.LeftRotateString(str,3);
    return 0;
}
