#include <iostream>
#include <vector>

using namespace std;

class Solution {
public:
    int minNumberInRotateArray(vector<int> rotateArray) { //寻找“旋转”过的有序数组中的最小数字
        if(rotateArray.empty()) return 0;
        //利用数组部分有序进行二分查找
        int p1=0,p2=rotateArray.size()-1;
        while(p2-p1>1){
            //※发现数组有序，返回第一个元素即可
            if(rotateArray[p1]<rotateArray[p2]) return rotateArray[p1];
            int nPosMid = (p2+p1)>>1;
            //首尾中三者相等，只能顺序查找了
            if(rotateArray[p1]==rotateArray[p2] && rotateArray[nPosMid]==rotateArray[p1]){
                return sequentialFind(rotateArray,p1,p2);
            }else if(rotateArray[nPosMid]>=rotateArray[p1]){
                p1=nPosMid;
            }else if(rotateArray[nPosMid]<=rotateArray[p2]){
                p2=nPosMid;
            } 
            
        }
        return rotateArray[p2];
    }
    int sequentialFind(vector<int> &vec,int nBegin,int nEnd){ //直接O(n)顺序查找
        if(vec.empty() || nBegin>nEnd || nEnd>vec.size()-1) return -1;
        int nMin=vec[nBegin];
        for(int i=nBegin+1;i<=nEnd;++i){
            if(vec[i]<nMin){
                nMin = vec[i];
            }
        }
        return nMin;
    }
};

int main(int argc, char const *argv[])
{
    Solution s;
    // 典型输入，单调升序的数组的一个旋转
    vector<int> test1 = { 3, 4, 5, 1, 2 };

    // 有重复数字，并且重复的数字刚好的最小的数字
    vector<int> test2 = { 3, 4, 5, 1, 1, 2 };

    // 有重复数字，但重复的数字不是第一个数字和最后一个数字
    vector<int> test3 = { 3, 4, 5, 1, 2, 2 };

    // 有重复的数字，并且重复的数字刚好是第一个数字和最后一个数字
    vector<int> test4 = { 1, 0, 1, 1, 1 };

    // ※※※※单调升序数组，旋转0个元素，也就是单调升序数组本身
    vector<int> test5 = { 1, 2, 3, 4, 5 };

    // 数组中只有一个数字
    vector<int> test6 = { 2 };
    int i;
    i = s.minNumberInRotateArray(test1);
    i = s.minNumberInRotateArray(test2);
    i = s.minNumberInRotateArray(test3);
    i = s.minNumberInRotateArray(test4);
    i = s.minNumberInRotateArray(test5);
    i = s.minNumberInRotateArray(test6);

    return 0;
}
