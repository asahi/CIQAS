#include <iostream>
#include <vector>
using namespace std;

class Solution
{
  public:
    vector<int> printMatrix(vector<vector<int>> matrix)
    {
        if (matrix.empty())
            return vector<int>();
        int rows = matrix.size();
        int columns = matrix.front().size();
        if (columns <= 0 || rows <= 0)
            return vector<int>();
        vector<int> vec;
        int nStart = 0;
        while (columns > nStart * 2 && rows > nStart * 2)
        {
            addCircle(vec, nStart, matrix);
            ++nStart;
        }
        return vec;
    }

  private:
    void addCircle(vector<int> &print, int nStart, vector<vector<int>> &matrix)
    {
        int rows = matrix.size();
        int columns = matrix.front().size();
        int endX = columns - 1 - nStart;
        int endY = rows - 1 - nStart;

        for (int i = nStart; i <= endX; ++i)
        {
            int number = matrix[nStart][i];
            print.push_back(number);
        }

        if (nStart < endY)
        {
            for (int i = nStart + 1; i <= endY; ++i)
            {
                int number = matrix[i][endX];
                print.push_back(number);
            }
        }

        if (nStart < endX && nStart < endY)
        {
            for (int i = endX - 1; i >= nStart; --i)
            {
                int number = matrix[endY][i];
                print.push_back(number);
            }
        }

        if (nStart < endX && nStart < endY - 1)
        {
            for (int i = endY - 1; i >= nStart + 1; --i)
            {
                int number = matrix[i][nStart];
                print.push_back(number);
            }
        }
    }
};

int main(int argc, char const *argv[])
{
    vector<vector<int> > vec;
    vec.push_back(vector<int>({1,2}));
    vec.push_back(vector<int>({3,4}));
    Solution s;
    auto ret = s.printMatrix(vec);
    return 0;
}
