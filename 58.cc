#include <string>

using namespace std;

class Solution {
    string ret;
public:
    string ReverseSentence(string str) {
        ret.clear();
        if(str.empty()) return ret;
        int length = str.size();
        char * mystr = new char [length+1];
        strncpy(mystr,str.c_str(),length);
        mystr[length]='\0';
        for(int i=0;i<length/2;++i){
            mystr[i]=mystr[i] ^ mystr[length-i-1];
            mystr[length-i-1]=mystr[i] ^ mystr[length-i-1];
            mystr[i]=mystr[i] ^ mystr[length-i-1];
        }
        int start = 0;
        for(int i=0;i<=length;++i){
            if(mystr[i]==' '||mystr[i]=='\0'){
                for(int j=start; j<start+(i-start)/2;++j){
                    mystr[j]=mystr[j]^mystr[i-(j-start)-1];
                    mystr[i-(j-start)-1]=mystr[j]^mystr[i-(j-start)-1];
                    mystr[j]=mystr[j]^mystr[i-(j-start)-1];
                }
                start=i+1;
            }
        }
        ret=mystr;
        delete[] mystr;
        return ret;
    }
};


int main(int argc, char const *argv[])
{
    string str="Beautiful World!";
    Solution s;
    string ret = s.ReverseSentence(str);
    return 0;
}
