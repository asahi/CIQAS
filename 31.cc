#include <vector>
#include <stack>
using namespace std;
class Solution {
private:
    stack<int> assistStack;
public:
    bool IsPopOrder(vector<int> pushV,vector<int> popV) {
        /*
            如果下一个弹出的数字是辅助栈的栈顶，直接弹出；
            如果不在栈顶，向下继续搜索压入序列，找到压入它和它之前的数字；若到结尾都没有找到，则false；
            弹出序列到结尾，则true
        */
        int j=0; //pushV
        for(int i=0; i<popV.size(); ++i){
            if(i!=0)
            //当前数字是栈顶，可以弹栈
            if(assistStack.top()==popV[i]){
                assistStack.pop();
                continue;
            }
            //不是栈顶，未入栈的序列中寻找
            for(; j<pushV.size(); ++j){
                    if(pushV[j]!=popV[i]){
                    assistStack.push(pushV[j]);
                }
                else{
                    //找到了，可以弹栈
                    assistStack.push(pushV[j]);
                    break;
                }
            }
            //入栈序列没找到这个数字
            if(j>=pushV.size()) return false;
            //当前数字是栈顶，可以弹栈
            if(assistStack.top()==popV[i]){
                ++j;
                assistStack.pop();
                continue;
            }
        }
        return true;
    }
};
int main(int argc, char const *argv[])
{
    vector<int> in={1,2,3,4,5};
    vector<int> out={4,5,3,2,1};//true
    vector<int> out2={4,5,3,1,2};//false
    vector<int> out3={3,5,4,2,1};//true
    Solution s;
    bool ret = s.IsPopOrder(in,out);
    return 0;
}
