#include <iostream>
using namespace std;

class Solution_ctor{
    private:
    static int answer;
    static int next;
    public:
    Solution_ctor(){
        answer+=next;
        next++;
    }
    static void Clean(){
        answer=0;
        next=1;
    }
    static int GetAnswer(){
        return answer;
    }

};
int Solution_ctor::answer=0;
int Solution_ctor::next=1;


class Solution {
public:
    int Sum_Solution(int n) {
        Solution_ctor::Clean();
        Solution_ctor * arr = new Solution_ctor[n];
        int ret = Solution_ctor::GetAnswer();
        delete[] arr;
        return ret;
    }
};