#include <vector>
using namespace std;

class Solution {
public:
    vector<int> multiply(const vector<int>& A) {
        if(A.empty()) return vector<int>();
        vector<int> output(A.size(),1);
        //C:A[0]***A[i-1]
        output[0] = 1; 
        for(int i = 1; i < A.size(); ++i)
        {
            output[i] = output[i - 1] * A[i - 1];
        }
        //D:A[i+1]***A[n-1]
        double temp = 1;
        for(int i = A.size() - 2; i >= 0; --i)
        {
            temp *= A[i + 1];
            output[i] *= temp;
        }
        return output;
    }
};

