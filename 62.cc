#include <list>
using namespace std;

class Solution {
public:
    list <int> children;
    int LastRemaining_Solution(int n, int m)
    {
        if(n<1 || m<1) return 0;
        int count=0;
        for(int i=0;i<n;++i)
            children.push_back(i);
        list<int>::iterator ite=children.begin();
        while(children.size()>1){
            if(count%m==m-1){
                ite=children.erase(ite);
                ite--;
            }
            count++;
            ite++;
            if(ite==children.end()){
                ite=children.begin();
            }
        }
        return children.front();
    }
};


int main(int argc, char const *argv[])
{
    Solution s;
    int cld = s.LastRemaining_Solution(5,3);
    return 0;
}
