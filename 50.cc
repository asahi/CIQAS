#include <cstdio>
#include <string>
using namespace std;

class Solution {
    unsigned int pHash[256];
public:
    int FirstNotRepeatingChar(string str) {
        if(str.empty()) return -1;
        const char *mystr = str.c_str();
        memset(pHash,0,sizeof(pHash));
        for(const char *ch = mystr;*ch!='\0';++ch){
            char c = *ch;
            if(c<0){
                c+=256;
            }
            pHash[c]++;
        }

        int pos=0;
        for(const char *ch = mystr;*ch!='\0';++ch,++pos){
            if(pHash[*ch]==1){
                return pos;
            }
        }
        return -1;
    }
};


int main(int argc, char const *argv[])
{
    string s="abaccdeff";
    Solution ss;
    ss.FirstNotRepeatingChar(s);
    return 0;
}
