class Solution {
public:
    bool isNumeric(char* string)
    {
        if(string == nullptr) return false;
        bool flag = scanInt(&string);

        if(*string == '.'){
            string++;
            flag = scanUnsignedInt(&string) || flag;
        }
        if(*string == 'e' || *string == 'E'){
            string++;
            flag = flag && scanInt(&string);
        }
        return flag && *string=='\0';
    }
private:
    bool scanInt(char **string){
        if(**string == '+' || **string == '-'){
            (*string)++;
        }

        return scanUnsignedInt(string);
    }
    bool scanUnsignedInt(char **string){
        char *orig = *string;
        while(**string!='\0' && **string >='0' && **string <='9'){
            (*string)++;
        }

        return orig<*string;
    }

};

int main(int argc, char const *argv[])
{
    Solution s;
    s.isNumeric("1a.34e7");
    return 0;
}
