#include <string>
using namespace std;
bool bStatus = false;
class Solution {
public:
    int StrToInt(string str) {
        if(str.empty()||str.c_str()==nullptr) return 0;
        const char *pStr = str.c_str();
        bool bNegative=false;
        long long result=0;
        if(*pStr=='-'){
            bNegative=true;
            pStr++;
        }else if(*pStr=='+'){
            pStr++;
        }
        for(;pStr!=nullptr&&*pStr!='\0';pStr++){
            if(*pStr>='0' && *pStr <='9'){
                result=result*10+*pStr-'0';
            }else{
                result=0;
                break;
            }
            if((bNegative==false && result>0x7FFFFFFFLL) || 
            (bNegative==true && result>0x80000000LL)){ //�������
                result=0;
                break;
            }
        }
        if(bNegative){
            result=0-result;
        }
        if(pStr!=nullptr && *pStr=='\0'){
            bStatus=true;
        }
        return (int)result;
    }
};

int main(int argc, char const *argv[])
{
    Solution s;
    int i=s.StrToInt(string("2147483647"));

    return 0;
}
