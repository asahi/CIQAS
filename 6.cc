#include<vector>
using namespace std;

 struct ListNode {
       int val;
       struct ListNode *next;
       ListNode(int x) :
             val(x), next(NULL) {
       }
 };

class Solution {
private:
    void printWorker(ListNode *head, vector<int> &results){
        if(!head) return;
        printWorker(head->next,results);
        results.push_back(head->val);
    }
public:
    vector<int> printListFromTailToHead(ListNode* head) { //基于递归的倒序遍历链表
        vector<int> results;
        printWorker(head,results);
        return results;
    }
};