#include <vector>
#include <set>

using namespace std;

class Solution {
    private:
    multiset<int,greater<int> > assistSet;   //STL可重复set(底层RBT实现)
    vector<int> ret;
public:
    vector<int> GetLeastNumbers_Solution(vector<int> input, int k) {
        assistSet.clear();
        ret.clear();
        if(input.size()<k) return ret;
        for(int i:input){
            if(assistSet.size()<k){
                assistSet.insert(i);
            }
            else{
                if(*(assistSet.begin()/*set中最大的元素位置*/)>i){
                    assistSet.erase(assistSet.begin());
                    assistSet.insert(i);
                }
            }
        }
        for(int i:assistSet){
            ret.push_back(i);
        }
        return ret;
    }
};