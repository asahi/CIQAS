#include <cstdio>
using namespace std;

struct TreeNode {
    int val;
    struct TreeNode *left;
    struct TreeNode *right;
    TreeNode(int x) :
            val(x), left(NULL), right(NULL) {
    }
};
class Solution {
public:
    TreeNode* KthNode(TreeNode* pRoot/*BST*/, int k)
    {
        if(pRoot==nullptr || k <1 ) return nullptr;
        return core(pRoot,k);
    }
    TreeNode *core(TreeNode *pNode, int &k){
        if(pNode==nullptr) return nullptr;
        TreeNode *result=nullptr;
        result=core(pNode->left,k);
        if(result==nullptr){
            if(k==1){
                result=pNode;
            }
            k--;
        }
        if(result==nullptr){
            result=core(pNode->right,k);
        }
        return result;
    }
};


int main(int argc, char const *argv[])
{
    /*��5��3��7��2��4��6��8��*/
    TreeNode *pNode = new TreeNode(5);
    TreeNode *pRoot = pNode;
    pNode->left=new TreeNode(3);
    pNode->right=new TreeNode(7);
    pNode->left->left=new TreeNode(2);
    pNode->left->right=new TreeNode(4);
    pNode->right->left=new TreeNode(6);
    pNode->right->right=new TreeNode(8);

    Solution s;
    TreeNode *p = s.KthNode(pRoot,3);
    return 0;
}
