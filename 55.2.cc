#include <cstdio>
struct TreeNode {
	int val;
	struct TreeNode *left;
	struct TreeNode *right;
	TreeNode(int x) :
			val(x), left(NULL), right(NULL) {
	}
};
class Solution1 {
public:
    int TreeDepth(TreeNode* pRoot)
    {
        if(pRoot==NULL) return 0;
        int depth_left=TreeDepth(pRoot->left);
        int depth_right=TreeDepth(pRoot->right);
        return depth_left>depth_right?depth_left+1:depth_right+1;
    }

    bool IsBalanced_Solution(TreeNode* pRoot) {  //AVL:它是一棵空树或它的左右两个子树的高度差的绝对值不超过1
        if(pRoot==NULL) return true;
        int dl=TreeDepth(pRoot);
        int dr=TreeDepth(pRoot);

        if(dl-dr<-1 || dl-dr>1) return false;

        return IsBalanced_Solution(pRoot->left)&&IsBalanced_Solution(pRoot->right);
    }
};


class Solution {
public:
    bool IsBalanced_Solution(TreeNode* pRoot) {
        int depth;
        return isbalanced(pRoot,&depth);
    }
    bool isbalanced(TreeNode *pRoot, int *depth){
        if(pRoot == NULL) {
            *depth=0;
            return true;
        }

        int dl,dr;
        if(isbalanced(pRoot->left,&dl)&&isbalanced(pRoot->right,&dr)){
            int delta = dl-dr;
            if(delta >= -1 && delta <=1){ //左右子树都要平衡
                *depth = (dl>dr?dl:dr) + 1;
                return true;
            }
            else return false;
        }
        else return false;
    }
};