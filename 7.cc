#include <iostream>
#include <vector>

using namespace std;

struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

class Solution {
public:
    TreeNode* reConstructBinaryTree(vector<int> pre,vector<int> vin) {
        if(!pre.size() || !vin.size()) return nullptr;

        return worker(&pre[0],&pre[pre.size()-1],&vin[0],&vin[vin.size()-1]);
    }
private:
    TreeNode* worker(int *preBegin, int *preEnd, int *inBegin, int *inEnd){
        int nRootValue = preBegin[0];
        TreeNode *pTree = new TreeNode(nRootValue);

        if(preBegin == preEnd){
            if(inBegin == inEnd)
                return pTree;
            else
                return nullptr;
        }

        int *pInRoot = inBegin;
        while(pInRoot<=inEnd && pInRoot[0]!=nRootValue){
            ++pInRoot;
        }
        //Can't find root position in InOrder
        if(pInRoot==inEnd && pInRoot[0]!=nRootValue) return nullptr;

        int nLengthLeft = pInRoot-inBegin;
        int *pEndLeft_Pre = preBegin+nLengthLeft;

        if(nLengthLeft > 0){
            pTree->left=worker(preBegin+1,pEndLeft_Pre,inBegin,inBegin+nLengthLeft-1);
        }

        if(inEnd-pInRoot > 0){
            pTree->right=worker(pEndLeft_Pre+1,preEnd,inBegin+nLengthLeft+1,inEnd);
        }
        return pTree;
    }    
};
int main() {
    vector<int> v1, v2;
    v1.push_back(1);
    v1.push_back(2);
    v1.push_back(4);
    v1.push_back(7);
    v1.push_back(3);
    v1.push_back(5);
    v1.push_back(6);
    v1.push_back(8);
    v2.push_back(4);
    v2.push_back(7);
    v2.push_back(2);
    v2.push_back(1);
    v2.push_back(5);
    v2.push_back(3);
    v2.push_back(8);
    v2.push_back(6);
    Solution s;
    auto ret = s.reConstructBinaryTree(v1, v2);
    
    return 0;
}
