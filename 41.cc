#include <vector>
#include <algorithm>
using namespace std;
/*
The push_heap function inserts a new value into the heap.
The pop_heap function swaps the first and last elements in
the heap specified by [First, Last), and then reduces the
length of the sequence by one before restoring the heap 
property. 
*/
class Solution {
public:
    vector<int> max;  //大顶堆   max[0]堆顶最大
    vector<int> min;  //小顶堆
    void Insert(int num)
    {
        if(((min.size() + max.size()) & 1) == 0) //已有偶数个元素 新数据进入最小堆
        {
            if(max.size() > 0 && num < max[0])
            {
                max.push_back(num);
                push_heap(max.begin(), max.end(), less<int>());
                // User-defined predicate function object that defines sense 
                // in which one element is less than another. A binary predicate
                // takes two arguments and returns true when satisfied and false
                // when not satisfied.

                num = max[0]; //最大值拿出来

                pop_heap(max.begin(), max.end(), less<int>());
                max.pop_back();
            }

            min.push_back(num);//放到小根堆  确保小根堆的元素都大于大根堆的
            push_heap(min.begin(), min.end(), greater<int>());
        }
        else
        {
            if(min.size() > 0 && min[0] < num)
            {
                min.push_back(num);
                push_heap(min.begin(), min.end(), greater<int>());

                num = min[0];

                pop_heap(min.begin(), min.end(), greater<int>());
                min.pop_back();
            }

            max.push_back(num);
            push_heap(max.begin(), max.end(), less<int>());
        }
    }

    double GetMedian()
    { 
        int size = min.size() + max.size();
        if(size == 0)
            return 0.0;

        double median = 0;
        if((size & 1) == 1)
            median = min[0];
        else
            median = (min[0] + max[0]) / 2.0;

        return median;
    }

};


int main(int argc, char const *argv[])
{
    /* code */
    return 0;
}
