#include <stdio.h>
#include <stdlib.h>
typedef struct node {
    int n;
    struct node *pNext;
} Node;

Node *CreateTempList() {
    Node *pHead = NULL;
    Node *pTemp = (Node *)calloc(1, sizeof(Node));
    pHead = pTemp;
    pTemp->n = 1;
    pTemp->pNext = (Node *)calloc(1, sizeof(Node));
    pTemp = pTemp->pNext;
    pTemp->n = 2;
    pTemp->pNext = (Node *)calloc(1, sizeof(Node));
    pTemp = pTemp->pNext;
    pTemp->n = 3;
    pTemp->pNext = (Node *)calloc(1, sizeof(Node));
    pTemp = pTemp->pNext;
    pTemp->n = 3;
    pTemp->pNext = (Node *)calloc(1, sizeof(Node));
    pTemp = pTemp->pNext;
    pTemp->n = 4;
    pTemp->pNext = (Node *)calloc(1, sizeof(Node));
    pTemp = pTemp->pNext;
    pTemp->n = 4;
    pTemp->pNext = (Node *)calloc(1, sizeof(Node));
    pTemp = pTemp->pNext;
    pTemp->n = 5;
    pTemp->pNext = NULL;

    return pHead;
}
void DelDupNode(Node **pHead){
    if(!pHead || !*pHead) return;
    Node *pNode=NULL;
    Node *pPreNode=NULL;
    Node *pNext=NULL;

    pNode=*pHead;
    while(pNode!=NULL){
        pNext=pNode->pNext;
        if(pNext!=NULL && pNode->n == pNext->n){ //需要删除
            int nDup=pNode->n;
            Node *pDel=pNode;
            while(pDel!=NULL && pDel->n==nDup){
            pNext=pDel->pNext;
            free(pDel);
            pDel=NULL;
            pDel=pNext;
            }
            if(pPreNode==NULL){ //该节点是头节点
                *pHead=pNext;
            }else{
                pPreNode->pNext=pNext;
            }
        pNode=pNext;
        }else{  // 不需要删除
            pPreNode=pNode;
            pNode=pNext;
        }
    }
}
int main(int argc, char const *argv[])
{
    Node *pHead = CreateTempList();
    DelDupNode(&pHead);
    return 0;
}
