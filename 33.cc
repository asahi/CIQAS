#include <vector>
using namespace std;

class Solution {
public:
    bool VerifySquenceOfBST(vector<int> sequence) {
        if(sequence.size()==0) return false;
        //if(sequence.size()==1) return true; 不写也行
        //最后一个元素是根节点
        //从头向后找左子树右子树的分界点
        int i=0;
        for(;i<sequence.size()-1;++i){
            //比较，直到找到比根节点大的（是右子树的开始）
            if(sequence[i]>sequence.back())
            {
                break;
            }
        }

        int j=i;
        for(;j<sequence.size()-1;++j){
            //在右子树里比较看有无比根节点小的，有就不是BST
            if(sequence[j]<sequence.back()){
                return false;
            }
        }
        bool retLeft=true,retRight=true;
        if(i>0){
        vector<int> left(sequence.begin(),sequence.begin()+i);
        retLeft = VerifySquenceOfBST(left);}
        if(i<sequence.size()-1){
        vector<int> right(sequence.begin()+i,sequence.end()-1);
        retRight = VerifySquenceOfBST(right);}
        return retLeft && retRight;
    }
};



int main(int argc, char const *argv[])
{
    Solution s;
    //            10
    //         /      \
    //        6        14
    //       /\        /\
    //      4  8     12  16
    vector<int> test1({4, 8, 6, 12, 16, 14, 10});  //Yes
    vector<int> test2({7, 4, 6, 5});  //No
    s.VerifySquenceOfBST(test1);
    s.VerifySquenceOfBST(test2);
    return 0;
}
