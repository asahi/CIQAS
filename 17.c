#include <stdio.h>
#include <stdlib.h>
#include <string.h>
int Increament(char *);
void Print(char *);
void Print1toMaxNDigits(int n);
void Print1toMaxNDigitsRec(char *numArr,int n,int index);
// void Print1toMaxNDigits(int n){
//     if(n<1) return;

//     char *pArr=(char *)malloc((n+1)*sizeof(char));
//     memset(pArr,'0',n); /*设置为字符'0'*/
//     pArr[n]='\0';

//     while(Increament(pArr)){
//         Print(pArr);
//     }

//     free(pArr);
// }
void Print1toMaxNDigits(int n){
    if(n<1) return;

    char *pArr=(char *)malloc((n+1)*sizeof(char));
    memset(pArr,'0',n); /*设置为字符'0'*/
    pArr[n]='\0';
    int i;
    for(i=0;i<=9;++i){
        pArr[0]=i+'0';/*以'0'为基础+i*/
        Print1toMaxNDigitsRec(pArr,n,0);
    }
    free(pArr);
}

void Print1toMaxNDigitsRec(char *numArr,int n,int index){
    if(index==n-1){ /*已经是最后一位*/
        Print(numArr);
        return;
    }
    for(int i=0;i<=9;++i){
        numArr[index+1]=i+'0';
        Print1toMaxNDigitsRec(numArr,n,index+1);
    }
}

void Print(char *numArr){
    if(!numArr) return;
    int isZero=1;
    size_t nLen=strlen(numArr);
    size_t i;
    for(i=0;i<=nLen;++i){
        if(isZero && numArr[i]!='0')
            isZero=0;
        if(!isZero)
            printf("%c",numArr[i]);
    }
    printf("\t");
}

int main(void){
    Print1toMaxNDigits(3);
    return 0;
}