#include <vector>
#include <cstdlib>
using namespace std;

int cmpfn (const void * a, const void * b){
        return *(int *)a - *(int *)b;
}
class Solution {
public:
    bool IsContinuous( vector<int> numbers ) {
        if(numbers.empty()) return false;
        qsort(&numbers[0],numbers.size(),sizeof(int),cmpfn);
        int zeroCount=0;
        for(int i=0;i<numbers.size();++i){
            if(numbers[i]==0){
                zeroCount++;
            }
            else{
                break;
            }
        }
        int low=zeroCount;
        for(int i=low+1;i<numbers.size();++i){
            if(numbers[i]==numbers[low]){//相等
                return false;
            }
            if(numbers[i]!=numbers[low]+1){ //不连续
                int delta = numbers[i]-numbers[low]-1;
                if(zeroCount > 0 && zeroCount >= delta){
                    zeroCount-=delta;
                }else{
                    return false;
                }
            }
            low=i;
        }
        return true;
    }
};


int main(int argc, char const *argv[])
{
    vector<int> v={1,3,2,5,4};
    Solution s;
    bool ret = s.IsContinuous(v);
    return 0;
}
