#include <cstdio>
#include <vector>
class Solution              //字符流中第一个不重复的字符
{
    std::vector<int> bucket;
    std::vector<char> inputs;
    /*  用例:
        "google"

        对应输出应该为:
        "ggg#ll"
    */

public:
    Solution():bucket(256,0){}
  //Insert one char from stringstream
    void Insert(char ch)
    {
        if(!bucket[ch]){
            inputs.push_back(ch);
        }
        bucket[ch]++;
    }
  //return the first appearence once char in current stringstream
    char FirstAppearingOnce()
    {
        if(inputs.empty()) return '#';
        for(char ch:inputs){
            if(bucket[ch]==1){
                return ch;
            }
        }
        return '#';
    }

};


int main(int argc, char const *argv[])
{
    setbuf(stdout,NULL);
    char str[]="google";
    Solution s;
    for(char *ch=str;*ch!='\0';++ch){
        s.Insert(*ch);
        putchar(s.FirstAppearingOnce());
    }
    return 0;
}
