#include <string>
#include <vector>
#include <cstdlib>
#include <cstring>
using namespace std;

const int k_maxStrLength = 10;

class Solution {
    static char cmpStr1[k_maxStrLength * 2+1];
    static char cmpStr2[k_maxStrLength * 2+1];
    static int compare( const void *arg1, const void *arg2);
public:
    string PrintMinNumber(vector<int> numbers) {
        if(numbers.empty()) return string();
        vector<char *> strArr(numbers.size(),nullptr);
        for(int i = 0; i<numbers.size(); ++i){
            strArr[i] = new char[k_maxStrLength+1];
            sprintf(strArr[i],"%d",numbers[i]);
        }
        qsort(&strArr[0],strArr.size(),sizeof(char *),compare);
        string ret;
        for(int i=0;i<strArr.size(); ++i){
            ret.append(strArr[i]);
            delete [] strArr[i];
        }
        return ret;
    }
};

char Solution::cmpStr1[k_maxStrLength * 2+1]={0};
char Solution::cmpStr2[k_maxStrLength * 2+1]={0};

int Solution::compare( const void *arg1, const void *arg2){
    strcpy(cmpStr1, *(const char **)arg1);
    strcat(cmpStr1, *(const char **)arg2);

    strcpy(cmpStr2, *(const char **)arg2);
    strcat(cmpStr2, *(const char **)arg1);

    return strcmp(cmpStr1, cmpStr2);
}

int main(int argc, char const *argv[])
{
    Solution s;
    vector<int> vec({3,5,1,4,2});
    string str = s.PrintMinNumber(vec);
    return 0;
}
