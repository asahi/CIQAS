#include <cstdio>
struct ListNode {
	int val;
	struct ListNode *next;
	ListNode(int x) :
			val(x), next(NULL) {
	}
};
class Solution {
int GetLength(ListNode *node){
    int nLength = 0;
    while(node){
        nLength++;
        node=node->next;
    }
    return nLength;
}
public:
    ListNode* FindFirstCommonNode( ListNode* pHead1, ListNode* pHead2) {
        if(!pHead1 || !pHead2 ) return nullptr;
        int len1=GetLength(pHead1);
        int len2=GetLength(pHead2);
        ListNode * plong=pHead1;
        ListNode * pshort=pHead2;
        int lendif=len1-len2;
        if(len1<len2){
            lendif=-lendif;
            plong=pHead2;
            pshort=pHead1;
        }
        for(int i=0;i<lendif;++i){
            plong=plong->next;
        }
        while(plong && pshort && plong!=pshort){
            if(plong->next==pshort->next)
            {
                return plong->next;
            }
            plong=plong->next;
            pshort=pshort->next;
        }
        return plong==nullptr?pshort:plong;
    }
};