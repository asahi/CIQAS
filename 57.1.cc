#include <vector>

using namespace std;

class Solution {
public:
    vector<int> ret;
    vector<int> FindNumbersWithSum(vector<int> array,int sum) {
        if(array.empty()) return ret;
        int start = 0;
        int end = array.size()-1;
        while(start<end){
            if(array[start]+array[end]>sum){
                end--;
            }else if(array[start]+array[end]<sum){
                start++;
            }else{
                ret.push_back(array[start]);
                ret.push_back(array[end]);
                break;
            }
        }
        return ret;
    }
};

int main(int argc, char const *argv[])
{
    vector<int> test = {1,2,4,7,11,15};
    Solution s;
    vector<int> ret = s.FindNumbersWithSum(test,15);
    return 0;
}
