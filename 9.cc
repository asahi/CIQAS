#include <stack>
using namespace std;
class Solution
{
public:
    void push(int node) {
        //只压入即可
        stack1.push(node);
    }

    int pop() {
        int ret=-1;
        //s2空 把s1全导出来压入，否则弹出
        if(stack2.empty()){
            while(!stack1.empty()){
                stack2.push(stack1.top());
                stack1.pop();
            }
        }
        if(!stack2.empty()){
            ret=stack2.top();
            stack2.pop();
        }
        return ret;
    }

private:
    stack<int> stack1;
    stack<int> stack2;
};