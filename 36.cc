#include <cstdio>

struct TreeNode
{
    int val;
    struct TreeNode *left;
    struct TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL)
    {
    }
};
class Solution
{
public:
    TreeNode *Convert(TreeNode *pRootOfTree)
    {
        if (pRootOfTree == nullptr)
            return nullptr;
        TreeNode *pLastNode = nullptr, *pHeadNode;
        CvtWorker(pRootOfTree, &pLastNode);
        pHeadNode = pLastNode;
        if (pHeadNode)
            while (pHeadNode->left) //从尾节点找回头节点
            {
                pHeadNode = pHeadNode->left;
            }
        return pHeadNode;
    }
    void CvtWorker(TreeNode *pRoot, TreeNode **pTail)
    {
        if (pRoot == nullptr || pTail == nullptr)
            return;
        if (pRoot->left)
        { //处理左子树
            CvtWorker(pRoot->left, pTail);
        }

        //根节点与左子树互相相连
        pRoot->left = *pTail;
        if (*pTail)
        {
            (*pTail)->right = pRoot;
        }

        *pTail = pRoot;

        if (pRoot->right)
        { //处理右子树
            CvtWorker(pRoot->right, pTail);
        }
    }
};