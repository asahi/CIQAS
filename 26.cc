#include <iostream>
using namespace std;

struct TreeNode {
	int val;
	struct TreeNode *left;
	struct TreeNode *right;
	TreeNode(int x) :
			val(x), left(NULL), right(NULL) {
	}
};
//题目描述
//输入两棵二叉树A，B，判断B是不是A的子结构。
//（ps：我们约定空树不是任意一个树的子结构）
class Solution {
    //如果node->val不是整数：
    // const double eps = 1e-8;
    // bool equ(double a,double b){
    //     return (a-b<eps || a-b>-eps);
    // }
public:
    bool HasSubtree(TreeNode* pRoot1, TreeNode* pRoot2)
    {
        bool bHasST=false;
        if(pRoot1 == nullptr || pRoot2 == nullptr) return false;
        //找到和目标树根节点相同的节点
        if(pRoot1->val == pRoot2->val){
        //遍历该子树确定相同
            bHasST = TreeSame(pRoot1,pRoot2);
        }
        if(!bHasST){
        //不同的话，往下继续找根节点相同的节点
            bHasST = HasSubtree(pRoot1->left,pRoot2);
            if(!bHasST) bHasST = HasSubtree(pRoot1->right,pRoot2);
        }
        return bHasST;
    }
    bool TreeSame(TreeNode *pRootChild, TreeNode *pCompareTarget){
        if(pCompareTarget == nullptr) return true;
        if(pRootChild == nullptr ) return false;
        bool bFlag=true;
        if(pRootChild->val == pCompareTarget->val) {
            bFlag = TreeSame(pRootChild->left,pCompareTarget->left) && TreeSame(pRootChild->right,pCompareTarget->right);
            return bFlag;
        }
        else{
            return false;
        }
    }
};