#include <cstdio>

class Solution {
    int Fibonacci(int n) { 
        if(n<=0) return 0;
        if(n==1) return 1;if(n==2) return 2;
        int fn_1=2;
        int fn_2=1;
        int fn;
        int i;
        for(i=3;i<=n;++i){
            fn=fn_1+fn_2;
            fn_2=fn_1;
            fn_1=fn;
        }
        return fn;
    }
public:
    int rectCover(int number) {
        return Fibonacci(number);
    }
};

int main(int argc, char const *argv[])
{
    Solution s;
    int ret = s.rectCover(4); // 5
    return 0;
}
