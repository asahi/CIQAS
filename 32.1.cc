#include <vector>
#include <deque>
using namespace std;
struct TreeNode {
	int val;
	struct TreeNode *left;
	struct TreeNode *right;
	TreeNode(int x) :
			val(x), left(NULL), right(NULL) {
	}
};
class Solution {
public:
    vector<int> PrintFromTopToBottom(TreeNode* root) {
        /*
            按层打印二叉树
            使用辅助队列从头取节点从后入节点
        */
        vector<int> ret;
        deque<TreeNode *> assistQueue;
        if(root==nullptr) return ret;
        assistQueue.push_back(root);
        while(!assistQueue.empty()){
            TreeNode *pNode = assistQueue.front();
            assistQueue.pop_front();
            ret.push_back(pNode->val);

            if(pNode->left!=nullptr){
                assistQueue.push_back(pNode->left);
            }
            if(pNode->right!=nullptr){
                assistQueue.push_back(pNode->right);
            }
        }
        return ret;
    }
};