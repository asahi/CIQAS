#include <deque>
#include <vector>
using namespace std;


class Solution {
    vector<int> ret;
public:
    vector<int> maxInWindows(const vector<int>& num, unsigned int size)
    {
        ret.clear();
        size_t length = num.size();
        if(size<1||length<size) return ret;
        deque<int> window;
        for(size_t i=0;i<size;++i){
            //处理前size个元素,不用检测范围超出,不用放入结果数组
            while(window.empty()==false && num[window.back()]<=num[i]){
                window.pop_back();
            }
            window.push_back(i);
        }
        for(size_t i=size;i<length;++i){
            ret.push_back(num[window.front()]);
            //当前元素比尾巴大,去尾
            while(window.empty()==false && num[window.back()]<=num[i]){
                window.pop_back();
            }
            //超出划定窗口的范围,去头
            while(window.empty()==false && (i - window.front()) >= size){
                window.pop_front();
            }
            window.push_back(i);
        }
        ret.push_back(num[window.front()]);
        return ret;
    }
};

