#include <cstdio>
class Solution {
public:
    int NumberOf1Between1AndN_Solution(int n)
    {
        //暴力解法：不断除以10，判断最后一位是不是1
        int nCount=0;
        for(int i=1;i<=n;++i){
            int num = i;
            while(num>0){
                if(num%10==1){
                    nCount++;
                }
                num/=10;
            }
        }
        return nCount;
    }
};

int main(int argc, char const *argv[])
{
    Solution s;
    for(int i=0;i<=111;++i){
    int ret = s.NumberOf1Between1AndN_Solution(i);
    printf("%d: count is %d\n",i,ret);}
    return 0;
}
