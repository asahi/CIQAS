#include <vector>
using namespace std;

class Solution {
public:
    int FindGreatestSumOfSubArray(vector<int> array) {
        if(array.empty()) return -1;
        int nSum=0;
        int nGreatest=int(0x80000000); //int类型最小值
        for(int i:array){
            if(nSum<0){ //前一个和是负数了，和当前相加一定不会更大
                nSum=i;
            }
            else{
                nSum+=i;
            }
            if(nSum>nGreatest) nGreatest=nSum;
        }
        return nGreatest;
    }
};
int main(int argc, char const *argv[])
{
    /* code */
    return 0;
}
