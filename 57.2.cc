#include <vector>
using namespace std;

class Solution {
    vector<vector<int> > ret;
public:
    vector<vector<int> > FindContinuousSequence(int sum) {
        ret.clear();
        if(sum < 2) return ret;
        int start = 1;
        int end = 2;
        int mysum = start + end;
        while(start < (sum + 1) / 2){
            if (mysum == sum){
                vector<int> vec;
                for(int i=start; i<=end; ++i){
                    vec.push_back(i);
                }
                ret.push_back(vec);
            }
            while((start < (sum + 1) / 2) && mysum > sum){
                mysum-=start;
                start++;
                if (mysum == sum){
                    vector<int> vec;
                    for(int i=start; i<=end; ++i){
                        vec.push_back(i);
                    }
                    ret.push_back(vec);
                }
            }
            end++;
            mysum+=end;
        }
    }
};
        // while(start<=(sum+1)/2){
        //     int mysum=0;
        //     for(int i=start; i<=end; ++i){
        //         mysum+=i;
        //     }
        //     if(mysum<sum){
        //         start++;
        //     }else if(mysum>sum){
        //         end--;
        //     }else{
        //         vector<int> vec;
        //         for(int i=start; i<=end; ++i){
        //             vec.push_back(i);
        //         }
        //     }
        // }