#include <vector>
#include <list>
#include <cstdio>
using namespace std;
struct TreeNode {
	int val;
	struct TreeNode *left;
	struct TreeNode *right;
	TreeNode(int x) :
		val(x), left(NULL), right(NULL) {
	}
};

/*
输入一颗二叉树的跟节点和一个整数，打印出二叉树中结
点值的和为输入整数的所有路径。路径定义为从树的根结
点开始往下一直到叶结点所经过的结点形成一条路径。(
注意: 在返回值的list中，数组长度大的数组靠前
*/
class Solution { //思路：辅助栈空间存储走过的节点值，前序递归
public:
	vector<vector<int> > ret;
	vector<vector<int> > FindPath(TreeNode* root, int expectNumber) {
		if (!root) return ret;
		int nCurrentSum=0;
		FPworker(root,expectNumber, nCurrentSum);
		return ret;
	}
	list<int> assist;
	void FPworker(TreeNode *pRoot, int expectNumber, int nCurrentSum) {
		nCurrentSum += pRoot->val;
		assist.push_back(pRoot->val);

		bool bIsLeaf = pRoot->left == nullptr && pRoot->right == nullptr;

		if (nCurrentSum == expectNumber && bIsLeaf) {
			vector<int> nowPath;
			for (list<int>::iterator ite = assist.begin(); ite != assist.end(); ++ite) {
				nowPath.push_back(*ite);
			}
			ret.push_back(nowPath);
		}

		else {
			if(pRoot->left)
				FPworker(pRoot->left, expectNumber, nCurrentSum);
			if(pRoot->right)
				FPworker(pRoot->right, expectNumber, nCurrentSum);
		}
		assist.pop_back();
	}
};