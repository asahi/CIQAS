#include <cstring>

class Solution {
public:
    void replaceSpace(char *str,int length) { //将一个字符串中的每个空格替换成“%20”
        if(!str || length <1) return; 
        //遍历一遍字符串，确定空格的个数
        int nSpaceCnt=0,i;
        for(i=0;i<length;++i){
            if(str[i]==' ') 
                ++nSpaceCnt;
        }
        //从后向前替换
        int j=i+nSpaceCnt*2;
        while((i>=0 && j>=0) && i!=j){
            if(str[i]!=' ')
                str[j--]=str[i--];
            else{
                str[j--]='0';
                str[j--]='2';
                str[j--]='%';
                i--;
            }
        }
    }
};


int main(int argc, char const *argv[])
{
    char str[20]="We Are Happy.";
    Solution s;
    s.replaceSpace(str,strlen(str));
    return 0;
}
