#include <iostream>
class Solution {
public:
    double Power(double base, int exponent){
        if(base == 0.0 && exponent == 0) return 0.0;
        if(exponent == 0) return 1.0;
        if(exponent == 1) return base;
        bool bNegative = false;
        if(exponent < 0)
            bNegative=true;
        exponent = (int)fabs(exponent);
        double ret = PowerUnsigned(base,exponent);
        if(bNegative){
            ret = 1.0/ret;
        }
        return ret;
    }
private:
    double PowerUnsigned(double base, unsigned int exponent) {
        if(base == 0.0) return 0.0;
        if(exponent == 1) return base;
        if(exponent == 0) return 1.0;
        /* f(a,n)=f(a,n/2)*f(a,n/2)     n��ż��
         * f(a,n)=f(a,n/2)*f(a,n/2)*a   n������
         */
        double result=0;
        result = PowerUnsigned(base,exponent>>1);
        result *= result;
        if(exponent & 1)
            result *= base;
        return result;
    }
};

int main(int argc, char const *argv[])
{
    Solution s;
    std::cout << s.Power(2,-3);
    return 0;
}
