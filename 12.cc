#include <cstring>
using namespace std;

class Solution {
    private:
    int total_rows;
    int total_cols;
    char *matrix;
    char *str;
    bool *visited;
public:
    bool hasPath(char* matrix, int rows, int cols, char* str)
    {
        if(matrix==NULL || rows<1 || cols<1||str==NULL) return false;
        this->matrix=matrix;
        total_rows=rows;
        total_cols=cols;
        this->str=str;

        visited=new bool[rows*cols];
        memset(visited,0,sizeof(bool)*(rows*cols));
        int nPos=0;
        for (int i = 0; i < rows; i++)
        {
            for (int j = 0; j < cols; j++)
            {
                //遍历每一个格子
                if(core(i,j,nPos)){
                    return true;
                }
            }
        }
        delete[] visited;
        return false;
    }
    bool core(int row,int col,int &currentLength){
        if(str[currentLength]=='\0') return true;

        bool hasPath=false;

        //判断满足条件
        if(row>=0 && col >=0 && row < total_rows && col < total_cols 
            && visited[row*total_cols+col]!=true 
            && matrix[row*total_cols+col]==str[currentLength])
        {
            currentLength++;
            visited[row*total_cols+col]=true;

            hasPath=core(row,col+1,currentLength) || //右
                    core(row+1,col,currentLength) || //下
                    core(row,col-1,currentLength) || //左
                    core(row-1,col,currentLength);   //上
            if(hasPath==false){
                currentLength--;
                visited[row*total_cols+col]=false;
            }
        }
        return hasPath;
    }
};
