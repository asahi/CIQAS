#include <vector>
using namespace std;

class Solution {
    long long worker(int *data, int *copy, long long start,long long end){
        if(start==end) {
            copy[start]=data[start];
            return 0;
        }
        long long length = (end-start)/2;

        long long left = worker(copy,data,start,start+length);
        long long right = worker(copy,data,start+length+1,end);
 
        long long i = start+length; //i是前半段最后元素的坐标
        long long j = end;          //j是后半段的

        long long indexCopy = end;
        long long count = 0;
        while (i>=start && j>=start+length+1)
        {
            if(data[i]>data[j]) //找到逆序对，复制前者（大）
            {
                copy[indexCopy]=data[i];
                indexCopy--; i--;
                count += j-(start+length); //后半段j前面的个数
            }else{              //不是逆序对，复制后者（大）
                copy[indexCopy]=data[j];
                indexCopy--; j--;
            }
        }
         
        //copy remain data (if has)
        for(;i>=start;--i){
            copy[indexCopy]=data[i];
            indexCopy--;
        }
        for(;j>=start+length+1;--j){
            copy[indexCopy]=data[j];
            indexCopy--;
        }

        return count + left + right;
    }
public:
    int InversePairs(vector<int> data) {
        if(data.empty()) return 0;
        int *copy = new int[data.size()];
        for(long long i = 0;i<data.size();++i){
            copy[i]=data[i];
        }
        long long count = worker(&data[0],copy,0,data.size()-1);
        delete[] copy;
        return count % 1000000007;
    }
};


int main(){
    vector<int> data={1,2,3,4,5,6,7,0};
    Solution s;
    int ret = s.InversePairs(data);
    return 0;
}