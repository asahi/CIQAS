#include <stack>
using namespace std;
class Solution {
public:
    void push(int value) {
        if(dataStack.empty())
        {
            dataStack.push(value);
            assistStack.push(value);
        }
        else{
            dataStack.push(value);
            if(value<assistStack.top()){
                assistStack.push(value);
            }
            else{
                assistStack.push(assistStack.top());
            }
        }
    }
    void pop() {
        dataStack.pop();
        assistStack.pop();
    }
    int top() {
        return dataStack.top();
    }
    int min() {
        return assistStack.top();
    }
private:
    stack<int> dataStack;
    stack<int> assistStack;
};


int main(int argc, char const *argv[])
{
    /* code */
    return 0;
}
