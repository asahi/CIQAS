#include <iostream>
using namespace std;
struct ListNode {
    int val;
    struct ListNode *next;
    ListNode(int x) :
        val(x), next(NULL) {
    }
};

class Solution {
public:
    ListNode* deleteDuplication(ListNode* pHead)
    {
        if(!pHead) return nullptr;
        ListNode *pRealHead = pHead;
        ListNode *pPre = nullptr;
        ListNode *pNode = pHead;
        ListNode *pNext = nullptr;
        while(pNode && pNode->next!=nullptr){
            bool needDel = false;
            pNext = pNode->next;
            if(pNext!=nullptr && pNext->val == pNode->val){
                needDel=true;
            }
            if(needDel){
                int val=pNext->val;
                ListNode *pDel=pNode;
                while(pDel!=nullptr && pDel->val==val){
                    pNext=pDel->next;
                    delete pDel;
                    pDel=pNext;
                }
                pNode=pNext;
                if(pPre!=nullptr)
                {
                    pPre->next=pNext;
                }else{
                    pRealHead=pNext;
                }
            }
            else{
                pPre=pNode;
                pNode=pNext;
            }
        }
        return pRealHead;
    }
};

int main(int argc, char const *argv[])
{
    ListNode *pHead = nullptr;
    ListNode *pNode = new ListNode(1);
    pHead = pNode;
    pNode->next = new ListNode(1);
    pNode=pNode->next;
    pNode->next = new ListNode(1);
    pNode=pNode->next;
    pNode->next = new ListNode(1);
    pNode=pNode->next;
    pNode->next = new ListNode(1);
    pNode=pNode->next;
    pNode->next = new ListNode(1);
    pNode=pNode->next;
    pNode->next = new ListNode(1);


    

    Solution s;
    auto ret = s.deleteDuplication(pHead);
    return 0;
}
