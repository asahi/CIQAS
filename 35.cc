#include <vector>
#include <list>
#include <map>
using namespace std;

struct RandomListNode {
	int label;
	struct RandomListNode *next, *random;
	RandomListNode(int x) :
			label(x), next(NULL), random(NULL) {
	}
};

class Solution {
public:
	map<RandomListNode *, RandomListNode *> assist;
	RandomListNode *pNew;
	RandomListNode* Clone(RandomListNode* pHead)
	{
		if (!pHead) return nullptr;
		CopypNext(pHead);
		ConnectpRandom(pHead);
		return pNew;
	}
	void CopypNext(RandomListNode *pHead) {
		if (!pHead) return;
		pNew = new RandomListNode(pHead->label);
		pNew->next = pHead->next;
		assist[pHead] = pNew;
		pHead = pHead->next;
		RandomListNode *pTemp = pNew;
		while (pHead) {
			pTemp->next = new RandomListNode(pHead->label);
			pTemp = pTemp->next;
			pTemp->next = pHead->next;
			assist[pHead] = pTemp;
			pHead = pHead->next;
		}
	}
	void ConnectpRandom(RandomListNode *pHead) {
		if (!pNew) return;

		RandomListNode *pNode = pNew,*pTemp = nullptr;
		while (pHead) {
			pNode->random = assist[pHead->random];
			pNode = pNode->next;
			pHead = pHead->next;
		}
	}
};


/*递归
作者：烤糊的蛋挞
链接：https ://www.nowcoder.com/questionTerminal/f836b2c43afc4b35ad6adc41ec941dba
来源：牛客网

class Solution {
public:
	map<RandomListNode*, RandomListNode*> nodemap;
	RandomListNode* Clone(RandomListNode* pHead)
	{
		if (pHead == NULL) return NULL;
		RandomListNode* phead = new RandomListNode(pHead->label);
		nodemap[pHead] = phead;
		phead->next = Clone(pHead->next);  //在这里递归是关键，保证所有节点都已生成，放入map
		phead->random = NULL;
		if (pHead->random) phead->random = nodemap[pHead->random];   //查找map中对应节点
		return phead;
	}
};*/

/*
链接：https://www.nowcoder.com/questionTerminal/f836b2c43afc4b35ad6adc41ec941dba
来源：牛客网
方法二：next指针关联
    创建新链表的时候，用原结点的next指针指向对应新结点，新结点的next指针指向
	下一个原结点，以此类推，形成之字形关联。然后，就可以先更新新链表的random
	指针，再解除next关联，更新next指针。这种方法不需要map来辅助，不管查找next
	还是random指针都是O(1)的，效率很高。

class Solution {
public:
    RandomListNode* Clone(RandomListNode* pHead)
    {
        if(pHead==NULL) return NULL;
 
        RandomListNode *newHead = new RandomListNode(pHead->label);
        RandomListNode *pHead1=NULL, *pHead2=NULL;
 
        // 上链，使新旧链表成之字形链接
        for(pHead1=pHead,pHead2=newHead;pHead1;){
            RandomListNode* tmp = pHead1->next;
            pHead1->next = pHead2;
            pHead2->next = tmp;
 
            // next
            pHead1 = tmp;
            if(tmp) pHead2 = new RandomListNode(tmp->label);
            else pHead2 = NULL;
        }
 
        // 更新新链表的random指针
        for(pHead1=pHead,pHead2=newHead;pHead1;){
            if(pHead1->random) pHead2->random = pHead1->random->next;
            else pHead2->random = NULL;
 
            pHead1 = pHead2->next;
            if(pHead1) pHead2 = pHead1->next;
            else pHead2 = NULL;
        }
 
        // 脱链，更新各链表的next指针
        for(pHead1=pHead,pHead2=newHead;pHead1;){
            pHead1->next = pHead2->next;
            if(pHead1->next) pHead2->next = pHead1->next->next;
            else pHead2->next = NULL;
 
            pHead1 = pHead1->next;
            pHead2 = pHead2->next;
        }
 
        return newHead;
    }
};
*/