#include <cstdio>

using namespace std;

class Solution {
    static int Min(int a, int b, int c){
        int ret;
        ret = (a<b)?a:b;
        ret = (ret<c)?ret:c;
        return ret;
    }
public:
    int GetUglyNumber_Solution(int index) {
        if(index < 1) return 0;
        int *pNumberArr = new int[index];
        pNumberArr[0]=1;
        int nextNum=1;

        int *pM2, *pM3, *pM5;
        pM2=pM3=pM5=pNumberArr;

        //用已有的丑数计算下一个丑数
        while(nextNum<index){
            int nMin=Min(*pM2 *2, *pM3*3, *pM5*5);
            pNumberArr[nextNum]=nMin;

            //找到已有的中最接近最大的那个
            while(*pM2*2<=pNumberArr[nextNum])
                pM2++;
            while(*pM3*3<=pNumberArr[nextNum])
                pM3++;
            while(*pM5*5<=pNumberArr[nextNum])
                pM5++;

            nextNum++;
        }


        int ret = pNumberArr[index-1];
        delete[] pNumberArr;
        return ret;
    }
};


int main(int argc, char const *argv[])
{
    Solution s;
    s.GetUglyNumber_Solution(5);
    return 0;
}
