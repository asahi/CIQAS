#include<cstdio>
struct TreeNode {
	int val;
	struct TreeNode *left;
	struct TreeNode *right;
	TreeNode(int x) :
			val(x), left(NULL), right(NULL) {
	}
};
class Solution {
public:
    int TreeDepth(TreeNode* pRoot)
    {
        if(pRoot==NULL) return 0;
        int depth_left=TreeDepth(pRoot->left);
        int depth_right=TreeDepth(pRoot->right);
        return depth_left>depth_right?depth_left+1:depth_right+1;
    }
};

int main(int argc, char const *argv[])
{
    TreeNode *pRoot=new TreeNode(1);
    pRoot->left=new TreeNode(2);
    pRoot->right=new TreeNode(3);
    TreeNode *pNode = pRoot->left;
    pNode->left=new TreeNode(4);
    pNode->right=new TreeNode(5);
    pNode->right->left=new TreeNode(6);
    Solution s;
    int ret = s.TreeDepth(pRoot);
    return 0;
}
