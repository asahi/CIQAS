#include <stdio.h>
#include <stdlib.h>
typedef struct node{
    int n;
    struct node *pNext;
} Node;

Node* CreateTempList(){
    Node *pHead = (Node *)calloc(1,sizeof(Node));
    Node *pTail=pHead;
    for(int i=1;i<=3;++i){
        Node *pNode = (Node *)calloc(1,sizeof(Node));
        pNode->n=i;
        pTail->pNext=pNode;
        pTail=pTail->pNext;
        pHead->n++;
    }
    return pHead;
}

void DelNode(Node **pHead, Node *pNode){
    if(!*pHead || !pNode) return;
    if(pNode->pNext) //不是尾节点
    {
        pNode->n=pNode->pNext->n;
        Node *pDel = pNode->pNext;
        pNode->pNext=pNode->pNext->pNext;
        free(pDel);
        pDel=NULL;
    }
    else if(*pHead == pNode){//是头节点也是尾节点，只有一个节点
        free(*pHead);
        *pHead=NULL;
    }
    //是尾节点，遍历到前一个节点位置，删除
    else{
        Node *pPrev=*pHead;
        while(pPrev->pNext!=pNode){
            pPrev = pPrev->pNext;
        }
        free(pPrev->pNext);
        pPrev->pNext=NULL;
    }
    pNode=NULL;
}

int main(int argc, char const *argv[])
{
    Node *pHead = CreateTempList();
    DelNode(&pHead,pHead->pNext->pNext);
    DelNode(&pHead,pHead->pNext->pNext);
    DelNode(&pHead,pHead);
    DelNode(&pHead,pHead);

    return 0;
}
