#include <vector>
using namespace std;

class Solution {
private:
    int GetFirstK(vector<int> &data, int k, int start, int end);
    int GetLastK(vector<int> &data, int k, int start, int end);
public:
    int GetNumberOfK(vector<int> data ,int k) {
        if(data.empty()) return 0;
        int nCount = 0;
        
        int first = GetFirstK(data, k, 0, data.size()-1);
        int last = GetLastK(data, k, 0, data.size()-1);

        if(first > -1 && last > -1){
            nCount = last-first+1;
        }
        
        return nCount;
    }
};
int Solution::GetFirstK(vector<int> &data, int k, int start, int end){
    if(start > end || start < 0 || end > data.size()-1) return -1;
    int half = start + (end - start) / 2;
    
    if(data[half]>k){
        end = half-1;
    }else if(data[half]<k)
    {
        start = half+1;
    }else{ //==k
        if(half>0 && data[half-1]!=k||half==0){
            return half; //got it
        }
        else{
            end=half-1;
        }
    }

    return GetFirstK(data,k,start,end);
}

int Solution::GetLastK(vector<int> &data, int k, int start, int end){
    if(start > end || start < 0 || end > data.size()-1) return -1;
    int half = start + (end-start) / 2;

    if(data[half] == k){
        if(half<data.size()-1 && data[half+1]!=k || half == data.size()-1){
            return half;
        }
        else
        {
            start = half+1;
        }
    }else if(data[half]<k){
        start = half+1;
    }
    else{ //>k
        end = half - 1;
    }

    return GetLastK(data, k, start, end);
}


int main(int argc, char const *argv[])
{
    vector<int> v={1, 3, 3, 3, 3, 4, 5};
    Solution s;
    int count = s.GetNumberOfK(v,2);
    
    return 0;
}
