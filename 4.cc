class Solution {
public:
    bool Find(int target, vector<vector<int> > array) { //有序二维数组中查找目标数字
        //思路:从右上角开始查找,比这个数字大的话排除当前行,比其小的话排除当前列
        int nLength = array[0].size(); //每个一维数组的长度相同
        //比这个数字大的话排除当前行
        for(int i=nLength-1,j=0;i>=0 && j<=array.size()-1;){
                if(target > array[j][i]){
                    j++;
                }
                else if(target < array[j][i]){
                    //比其小的话排除当前列
                    i--;
                }
                else{
                    //找到了
                    return true;
            }
        }
        return false;
    }
};