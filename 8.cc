#include <cstddef>
using namespace std;


struct TreeLinkNode {
    int val;
    struct TreeLinkNode *left;
    struct TreeLinkNode *right;
    struct TreeLinkNode *next;
    TreeLinkNode(int x) :val(x), left(NULL), right(NULL), next(NULL) {
        
    }
};

class Solution {
public:
    TreeLinkNode* GetNext(TreeLinkNode* pNode)
    {
        if(pNode == nullptr) return nullptr;

        if(pNode->right!=nullptr)//有右子树
        {//找右的最左
            pNode=pNode->right;
            while(pNode->left!=nullptr){
                    pNode=pNode->left;
                }
                return pNode;
        }
        else{                   //没有右
            if(pNode->next==nullptr) //没有父节点
            {
                return nullptr;
            }
            if(pNode==pNode->next->left) //是父节点的左孩子
            {
                return pNode->next;
            }else if(pNode==pNode->next->right){ //是父节点的右孩子
                //找父亲为左孩子的那个节点,返回其父亲
                while(pNode->next!=nullptr){
                    pNode=pNode->next;
                    if(pNode->next==nullptr){
                        return nullptr;
                    }else if(pNode==pNode->next->left){
                        return pNode->next;
                    }else continue;
                }
                return nullptr;
            }
        }
        return nullptr;
    }
};