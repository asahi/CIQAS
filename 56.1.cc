#include <cstdio>
#include <vector>
using namespace std;

class Solution {
public:
    void FindNumsAppearOnce(vector<int> data,int* num1,int *num2) {
        if(data.empty())return;
        int temp=data[0];
        for(int i=1;i<data.size();++i){
            temp=temp^data[i];
        }
        if(!temp) return;
        unsigned int bit=1;
        while((bit&temp)==0){ //找到最右面的为1的位
            bit=bit<<1;
        }
        vector<int> v1,v2;
        for(int i:data){
            if((i&bit)!=0){
                v1.push_back(i);
            }
            else{
                v2.push_back(i);
            }
        }
        if(v1.empty()||v2.empty()) return;
        *num1=v1[0];
        for(int i = 1;i<v1.size();++i){
            *num1=*num1^v1[i];
        }
        *num2=v2[0];
        for(int i = 1;i<v2.size();++i){
            *num2=*num2^v2[i];
        }
    }
};

int main(int argc, char const *argv[])
{
    vector<int> test({1,1,2,2,4,4,7,3,5,7,9,9});
    Solution s;
    int ret[2];
    s.FindNumsAppearOnce(test,&ret[0],&ret[1]);
    return 0;
}
