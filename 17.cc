#include <iostream>
#include <cstring>
using namespace std;

class Solution{
public:
    void print1toNdigits(int n){
        if(n<1) return;
        char *pNumber = new char[n+1];
        memset(pNumber,0,sizeof(char)*(n+1));

        //最高位从0到9
        for(int i=0;i<10;++i){
            pNumber[0]=i+'0';
            printRecWorker(pNumber,n,0);
        }
        delete [] pNumber;
    }
private:
    void printRecWorker(char number[], int nLength, int lastPos){
        if(!number || nLength < 1 ) return;
        if(lastPos==nLength-1) printer(number);
        else
        for(int i=0;i<10;++i){
            number[lastPos+1]=i+'0';
            printRecWorker(number,nLength,lastPos+1);
        }
        
    }
    void printer(char buffer[]){
        if(!buffer) return;
        bool zero_mark=true;
        for(int i=0;i<strlen(buffer);++i){
            if(zero_mark==true && buffer[i]!='0'){
                zero_mark=false;
            }
            if(zero_mark==false)
                cout << buffer[i];
        }
        if(!zero_mark) cout << '\t';
    }
};


int main(int argc, char const *argv[])
{
    Solution s;
    s.print1toNdigits(3);
    return 0;
}
