#include <vector>
#include <queue>
#include <set>
using namespace std;
class Solution {
    set<string> ret;
    vector<string> ret2;
public:
    vector<string> Permutation(string str) {
        if(str.empty()) return ret2;
        char *mystr = new char[str.size()+1];
        strcpy(mystr,str.c_str());
        Permutation(mystr,mystr);
        delete []mystr;
        for(auto i : ret){
            ret2.push_back(i);
        }
        return ret2;
    }
    void Permutation(char *str, char *pos){
        if(!str || !pos) return;
        if(*pos=='\0') {
            ret.insert(string(str));
            return;
        }
        char ctemp;
        
        for(char *i = pos; *i!='\0'; ++i){
            ctemp=*i;
            *i=*pos;
            *pos=ctemp;

            Permutation(str,pos+1);

            ctemp=*i;
            *i=*pos;
            *pos=ctemp;
        }
    }
};


int main(int argc, char const *argv[])
{
    Solution s;
    string str("123");
    s.Permutation(str);
    return 0;
}
